require 'optparse'
require 'csv'
require 'google_drive'
require_relative './ethicontrol-utils/lib/ethicontrol/utils'

DEFAULT_SPREADSHEET = ''
DEFAULT_FOLDER = 'locales'
DEFAULT_LOCALES = 'ru,en'
DEFAULT_WORKSHEET = 'worksheet'
DEFAULT_CONFIG = 'config.json'

def default_options
  {
    spreadsheet: DEFAULT_SPREADSHEET,
    folder: DEFAULT_FOLDER,
    locales: DEFAULT_LOCALES,
    worksheet: DEFAULT_WORKSHEET,
    config: DEFAULT_CONFIG
  }
end

def parse_options
  options = default_options

  OptionParser.new do |opts|
    opts.banner = 'Usage: ruby [-s spreadsheet] [-f folder] [-l locales] [-w title] [-c config_file] export.rb'
    opts.on('-s [spreadsheet]', '--spreadsheet [spreadsheet]', "Google spreadsheet key, default: #{DEFAULT_SPREADSHEET}") do |spreadsheet|
      options[:spreadsheet] = spreadsheet
    end
    opts.on('-f [folder]', '--folder [folder]', "translations folder, default: #{DEFAULT_FOLDER}") do |folder|
      options[:folder] = folder
    end
    opts.on('-l [locales]', '--locales [locales]', "translations locales, default: #{DEFAULT_LOCALES}") do |locales|
      options[:locales] = locales
    end
    opts.on('-w [title]', '--worksheet [title]', "worksheet title, default: #{DEFAULT_WORKSHEET}") do |worksheet|
      options[:worksheet] = worksheet
    end
    opts.on('-c [config_file]', '--config [config_file]', "path to google config file, default: #{DEFAULT_CONFIG}") do |config|
      options[:config] = config
    end
    opts.on('-v', '--version', 'show version') do
      puts Ethicontrol::Utils::VERSION
      exit
    end
    opts.on('-h', '--help', 'Prints this help') do
      puts opts
      exit
    end
  end.parse!
  options
end
