require_relative './options'

options = parse_options

session = GoogleDrive::Session.from_service_account_key options[:config]

Ethicontrol::Utils::Google.download(
  options[:worksheet],
  session: session,
  spreadsheet_key: options[:spreadsheet],
  folder: options[:folder]
)

source = "#{options[:folder]}/#{options[:worksheet]}.csv"
data = CSV.read(source, headers: true).map(&:to_hash)
options[:locales].split(',').each do |lang|
  hash = Ethicontrol::Utils::I18n::Hash.build(data, lang: lang)
  hash.save("#{options[:folder]}/#{lang}.yml")
end
