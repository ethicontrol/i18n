require_relative './options'

options = parse_options

def create_worksheet(title, content, session:, spreadsheet_key:)
  spreadsheet = Ethicontrol::Utils::Google::Spreadsheet.find(spreadsheet_key, session: session)
  Ethicontrol::Utils::Google::Worksheet.create! spreadsheet, title, content
end

collection = Ethicontrol::Utils::I18n::Collection.new
options[:locales].split(',').each do |lang|
  filepath = "#{options[:folder]}/#{lang}.yml"
  yaml = Ethicontrol::Utils::I18n::Hash.load(filepath)
  collection.add_translations lang => yaml.flat_hash
end

session = GoogleDrive::Session.from_service_account_key options[:config]
title = "#{options[:worksheet]} #{Time.now}"
worksheet = create_worksheet(title, collection.to_csv, session: session, spreadsheet_key: options[:spreadsheet])
google_url = Ethicontrol::Utils::Google.url(spreadsheet_key: options[:spreadsheet], session: session, gid: worksheet.gid)
puts google_url
