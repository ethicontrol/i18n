module Ethicontrol
  module Utils
    module Google
      def self.download(title, spreadsheet_key:, session: google_session, folder: nil, filename: nil)
        normalized_title = title.split('.').last&.downcase
        folder ||= 'tmp'
        filename ||= "#{normalized_title}.csv"
        filepath = [folder, filename].join('/')

        Worksheet.find(title, spreadsheet_key: spreadsheet_key, session: session)&.download_to(filepath)
      end

      def self.url(session:, spreadsheet_key:, gid: nil)
        Spreadsheet.find(spreadsheet_key, session: session)&.url(gid)
      end
    end
  end
end
