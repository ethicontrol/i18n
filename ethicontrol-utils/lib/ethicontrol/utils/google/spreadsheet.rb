module Ethicontrol
  module Utils
    module Google
      class Spreadsheet
        def initialize(spreadsheet)
          @spreadsheet = spreadsheet
        end

        attr_reader :spreadsheet

        delegate :worksheet_by_gid, :worksheet_by_title, :add_worksheet, :worksheets, :key, to: :spreadsheet

        # rubocop:disable Style/ClassVars
        def self.find(spreadsheet_key, session:)
          @@cache ||= {}
          @@cache[spreadsheet_key] ||= new session.spreadsheet_by_key(spreadsheet_key)
        end

        def self.flush_cache
          @@cache = {}
        end
        # rubocop:enable Style/ClassVars

        def find_worksheet(options)
          find_worksheet_by_title(options[:title]) ||
            !options[:silent] && raise(Worksheet::NotFoundError, options)
        end

        def find_worksheet_by_title(title)
          worksheet = worksheet_by_title(title) if title
          Worksheet.new(worksheet) if worksheet
        end

        def cached_worksheets
          @_cached_worksheets ||= worksheets
        end

        def worksheet_by_title(title)
          cached_worksheets.find { |ws| ws.title == title }
        end

        def worksheet_by_gid(gid)
          cached_worksheets.find { |ws| ws.sheet_id == gid.to_i }
        end

        def create_worksheet(title)
          Worksheet.new add_worksheet(title)
        end

        def url(gid = nil)
          gid_param = "#gid=#{gid}" if gid
          "https://docs.google.com/spreadsheets/d/#{key}/edit#{gid_param}"
        end
      end
    end
  end
end
