module Ethicontrol
  module Utils
    module Google
      class Worksheet
        class NotFoundError < StandardError; end

        def initialize(worksheet)
          @worksheet = worksheet
        end

        def self.find(title, session:, **options)
          spreadsheet = Spreadsheet.find(options[:spreadsheet_key], session: session)
          worksheet = spreadsheet.find_worksheet(options.merge(title: title, silent: true))
          return worksheet if worksheet

          raise(NotFoundError, title)
        end

        def self.create!(spreadsheet, title, content)
          spreadsheet.create_worksheet(title).tap do |worksheet|
            worksheet.update_cells(1, 1, content)
            worksheet.save
          end
        end

        attr_reader :worksheet

        delegate :spreadsheet, :title, :gid, :save, :update_cells, :present?, to: :worksheet

        def ==(other)
          worksheet == other.worksheet
        end

        def url
          Spreadsheet.new(spreadsheet).url(gid)
        end

        def download_to(file)
          puts 'Downloading...'
          worksheet.export_as_file(file).tap do |result|
            puts "Saved to #{file}" if result
          end
        end
      end
    end
  end
end
