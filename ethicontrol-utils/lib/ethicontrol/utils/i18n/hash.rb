module Ethicontrol
  module Utils
    module I18n
      class Hash
        include Comparable

        def initialize(lang, hash)
          @lang = lang.to_s
          @nested = hash
        end

        def self.load(file)
          hash = YAML.load_file(file)
          new hash.keys.first, hash.values.first
        end

        def self.build(data, lang:)
          hash = {}
          data.each do |row|
            next if row['key'].blank?

            hash = process_nested(hash, row['key'], row[lang]) || {}
          end
          new lang, hash
        end

        def self.process_nested(hash, key, value)
          hash ||= {}
          if key.include?('.')
            keys = key.partition('.')
            hash.merge(keys.first => process_nested(hash[keys.first], keys.last, value))
          else
            hash.merge(key => value)
          end
        end

        def <=>(other)
          hash <=> other.hash
        end

        def hash
          @nested
        end

        def flat_hash
          @_flat_hash ||= to_flat_hash
        end

        def save(file)
          File.write(file, to_yml)
        end

        def to_yml(save_to: false)
          { lang => nested }.to_yaml.gsub(" \n", "\n").tap do |yml|
            save(save_to, yml) if save_to
          end
        end

        private

        attr_reader :lang, :nested

        def to_flat_hash
          @_flat_hash = {}
          flatten hash
          @_flat_hash
        end

        def flatten(nested, keys = [])
          nested.each do |key, value|
            subkeys = keys + [key]
            current_key = subkeys.join('.')
            case value.class.name
            when 'Hash'
              flatten(value, subkeys)
            when 'Array'
              @_flat_hash[current_key] = value.join("\n\n")
            when 'String', 'Integer', 'FalseClass', 'TrueClass'
              @_flat_hash[current_key] = value
            when 'NilClass'
              @_flat_hash[current_key] = ''
            else
              raise "Unknown class #{value.class.name} in #{@lang}, key: #{current_key}"
            end
          end
        end
      end
    end
  end
end
