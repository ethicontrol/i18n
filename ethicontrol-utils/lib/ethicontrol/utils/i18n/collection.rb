module Ethicontrol
  module Utils
    module I18n
      class Collection
        def initialize(translations = {})
          @translations = {}
          add_translations(translations)
        end

        attr_reader :translations

        def add_translations(translations)
          @translations.merge!(translations.stringify_keys.compact)
        end

        def to_csv(save: false)
          ([header] + rows.values).tap do |result|
            write(result, file: save) if save
          end
        end

        private

        def header
          ['key'] + translations.keys
        end

        def rows
          hash_of_rows = {}
          translations.each.with_index(1) do |(_lang, translation), index|
            translation.each do |key, value|
              hash_of_rows[key] ||= [key]
              hash_of_rows[key][index] = value
            end
            hash_of_rows.each do |key, row|
              hash_of_rows[key][index] = nil if row.size < index + 1
            end
          end
          hash_of_rows
        end

        def write(result, file:)
          File.write(file, result.map(&:to_csv).join)
        end
      end
    end
  end
end
