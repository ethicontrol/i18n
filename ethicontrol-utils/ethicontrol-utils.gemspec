lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ethicontrol/utils/version'

Gem::Specification.new do |spec|
  spec.name          = 'ethicontrol-utils'
  spec.version       = Ethicontrol::Utils::VERSION
  spec.authors       = ['Andrew Garshyn']
  spec.email         = ['andrew@ethicontrol.com']

  spec.summary       = 'ethicontrol utilities'
  spec.description   = 'library for shared classes'
  spec.homepage      = 'https://ethicontrol.com'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.5.0'
  spec.add_dependency 'activesupport'
  spec.add_dependency 'google_drive', '3.0.5'
  spec.add_dependency 'rubocop', '1.5.2'
  spec.add_dependency 'rubocop-performance', '1.9.1'
  spec.add_dependency 'rubocop-rails', '2.8.1'
  spec.add_dependency 'rubocop-rake', '0.5.1'
  spec.add_dependency 'rubocop-rspec', '2.0.1'
  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'simplecov'
end
